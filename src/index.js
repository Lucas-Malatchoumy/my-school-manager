import './index.scss';
import Dashboard from './pages/dashboard';
import Pomo from './pages/promo';
import P404 from './pages/404';
import Student from './pages/student';
import Update from './pages/update';
import Register from './pages/register';
import Connexion from './pages/connexion';

const Routes = class Routes {
  getParams = () => {
    const urlParams = window.location.search.split('?')[1].split('=');
    const data = {};
    for (let i = 0; i < urlParams.length; i += 2) {
      const counter = i + 1;

      Object.assign(data, { [urlParams[i]]: urlParams[counter] });
    }
    return data;
  };

  getPath() {
    const arrayPath = window.location.pathname.split('/');

    arrayPath.shift();

    console.log(arrayPath.join('/'));
    if (window.location.search) {
      return {
        path: arrayPath.join('/'),
        params: this.getParams()
      };
    }
    return {
      path: arrayPath.join('/')
    };
  }

  run() {
    const { path, params } = this.getPath();
    const dashboard = new Dashboard();
    const promo = new Pomo(params);
    const p404 = new P404();
    const student = new Student(params);
    const update = new Update(params);
    const register = new Register(params);
    const connexion = new Connexion(params);
    console.log(path, params);

    if (params) {
      switch (path) {
        case 'dashboard':
          dashboard.run();
          break;
        case 'promo':
          promo.run();
          break;
        case 'student':
          student.run();
          break;
        case 'update':
          update.run();
          break;
        default:
          p404.run();
      }
    } else {
      switch (path) {
        case 'register':
          register.run();
          break;
        case 'connexion':
          connexion.run();
          break;
        default:
          dashboard.run();
      }
    }
  }
};

const routes = new Routes();
routes.run();
