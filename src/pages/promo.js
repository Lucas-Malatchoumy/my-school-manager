import axios from 'axios';

const Promo = class {
  constructor(params) {
    this.el = document.querySelector('#app');
    this.params = params;
  }

  renderUsers(student) {
    const {
      id, firstName, lastName, age, promo, speciality, gender
    } = student;
    return `
    <tr>
      <th scope="row">${id}</th>
      <td>${firstName}</td>
      <td>${lastName}</td>
      <td>${age}</td>
      <td>${promo}</td>
      <td>${speciality}</td>
      <td>${gender}</td>
      <td><a href="/student?id=${id}"><button type="button" class="btn btn-success">See the profile</button></a></td>
    </tr>`;
  }

  renderTable() {
    return `
    <table class="table table-dark">
    <thead>
      <tr>
        <th scope="col">Id</th>
        <th scope="col">FirstName</th>
        <th scope="col">LastName</th>
        <th scope="col">Age</th>
        <th scope="col">Promo</th>
        <th scope="col">Speciality</th>
        <th scope="col">Gender</th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody class="item">
    </tbody>
  </table>`;
  }

  getClass() {
    const item = document.querySelector('.item');
    console.log(item);
    const config = {
      method: 'get',
      url: `https://3f3j7irda8.execute-api.eu-west-3.amazonaws.com/my-school-manager-show/users/show?promo=${this.params.promo}`,
      headers: {
        'x-api-key': 'eILWZmmK9Eap08ZwchAXS0mGnei11tl43jruta00'
      }
    };

    axios(config)
      .then((response) => {
        if (response.data !== '') {
          const classe = response.data;
          classe.forEach((student) => {
            item.innerHTML += this.renderUsers(student);
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  run() {
    this.el.innerHTML = this.renderTable();
    this.getClass();
  }
};

export default Promo;
