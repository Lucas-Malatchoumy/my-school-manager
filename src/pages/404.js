const P404 = class {
  constructor() {
    this.el = document.querySelector('#app');
  }

  render() {
    return `
        <h1> P404 </h1>`;
  }

  run() {
    this.el.innerHTML = this.render();
  }
};

export default P404;
