import axios from 'axios';

const Register = class {
  constructor() {
    this.el = document.querySelector('#app');
  }

  register() {
    const error1 = document.querySelector('.error-pwd');
    const btnEl = document.querySelector('#submit');
    const input = document.querySelectorAll('input');
    console.log(input.value);
    btnEl.addEventListener('click', () => {
      error1.classList.remove('active');
      const firstName1 = document.querySelector('#firstName').value;
      const lastName1 = document.querySelector('#lastName').value;
      console.log(lastName1);
      const promo1 = document.querySelector('#promo').value;
      const gender1 = document.querySelector('#gender').value;
      const speciality1 = document.querySelector('#speciality').value;
      const birthdayDate = document.querySelector('#birthdayDate').value;
      const email1 = document.querySelector('#email').value;
      const password1 = document.querySelector('#password').value;
      const confirmPassword = document.querySelector('#confirm-password').value;
      if (password1 === confirmPassword) {
        const data = JSON.stringify({
          id: (Math.random() * (999 - 1) + 1).toString(),
          firstName: firstName1,
          lastName: lastName1,
          age: birthdayDate,
          gender: gender1,
          promo: promo1,
          speciality: speciality1,
          email: email1,
          password: password1
        });
        const config = {
          method: 'post',
          url: 'https://3f3j7irda8.execute-api.eu-west-3.amazonaws.com/my-school-manager-show/users/add',
          headers: {
            'x-api-key': 'eILWZmmK9Eap08ZwchAXS0mGnei11tl43jruta00',
            'Content-Type': 'application/json'
          },
          data
        };

        axios(config)
          .then((response) => {
            console.log(JSON.stringify(response.data));
          })
          .catch((error) => {
            console.log(error);
          });
      } else {
        error1.classList.add('active');
      }
    });
  }

  render() {
    return `
    <section class="vh-200 gradient-custom">
    <div class="container py-5 h-50">
      <div class="row justify-content-center align-items-center h-100">
        <div class="col-12 col-lg-9 col-xl-7">
          <div class="card shadow-2-strong card-registration" style="border-radius: 15px;">
            <div class="card-body p-4 p-md-5">
              <div class="mb-4 pb-2 pb-md-0 mb-md-5 choice">
                <div>
                  <h3>Inscription</h3>
                </div>
                <div class="connexion">
                  <h3>Connexion</h3>
                  <a href="/connexion"<button type="button" class="btn btn-success modal-trigger getUser mt-3">Connexion</button></a>
                </div>
              </div>
              <form>
  
                <div class="row">
                  <div class="col-md-6 mb-4">
  
                    <div class="form-outline">
                     <label class="form-label" for="firstName">First Name</label>
                      <input type="text" id="firstName" required class="form-control" />
                    </div>
  
                  </div>
                  <div class="col-md-6 mb-4">
  
                    <div class="form-outline">
                      <label class="form-label" for="lastName">Last Name</label>
                      <input type="text" id="lastName" class="form-control" />
                    </div>
  
                  </div>
                </div>
  
                <div class="row">
                  <div class="col-md-6 mb-4 d-flex align-items-center">
  
                    <div class="form-outline datepicker w-100">
                    <label for="birthdayDate" class="form-label">Birthday</label>
                      <input
                        type="date"
                        class="form-control"
                        id="birthdayDate"
                      />
                    </div>
  
                  </div>
                  <div class="col-md-6 mb-4">
                    <h6 class="mb-2 pb-1">Gender: </h6>  
                    <div class="row">
                  <div class="col-12">
                    <select id="gender" class="select form-control">
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                      <option value="Other">Other</option>
                    </select>  
                  </div>
                </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 mb-4">
  
                    <div class="form-outline">
                     <label class="form-label" for="promo">Promo</label>
                      <input type="text" id="promo" class="form-control" />
                    </div>
  
                  </div>
                  <div class="col-md-6 mb-4">
  
                    <div class="form-outline">
                      <label class="form-label" for="speciality">Speciality</label>
                      <input type="text" id="speciality" class="form-control" />
                    </div>
  
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6 mb-4 pb-2">

                  <div class="form-outline">
                  <label class="form-label" for="emailAddress">Email</label>
                    <input type="email" id="email" class="form-control" />
                  </div>

                </div>
                <div class="col-md-6 mb-4 pb-2">

                  <div class="form-outline">
                    <label class="form-label" for="password">Password</label>
                    <input type="password" id="password" class="form-control" />
                  </div>

                </div>
                <div class="row">
                <div class="col-md-6 mb-4 pb-2">
                  <div class="form-outline">
                  </div>
                </div>
                <div class="col-md-6 mb-4 pb-2">

                  <div class="form-outline">
                    <label class="form-label" for="password">Confirm password</label>
                    <input type="password" id="confirm-password" class="form-control" />
                    <label class="form-label error-pwd" for="password">Password are not the same</label>
                  </div>

                </div>
                <div class="mt-4 pt-2">
                  <input id="submit" class="btn btn-primary btn-lg" value="Submit" />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>`;
  }

  run() {
    this.el.innerHTML = this.render();
    this.register();
  }
};

export default Register;
