const Dashboard = class {
  constructor() {
    this.el = document.querySelector('#app');
  }

  render() {
    return `
    <h1 text-center> Welcome to MySchool Manager ! </h1>
    <div class="classes">
        <div class="card mt-5" style="width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">B3 promo</h5>
          <p class="card-text">Developper, Designer and Marketer !</p>
          <a href="/promo?promo=B3" class="card-link">go to the class !</a>
        </div>
      </div>
      <div class="card mt-5" style="width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">B2 promo</h5>
          <p class="card-text">Polyvalent student !</p>
          <a href="/promo?promo=B2" class="card-link">go to the class !</a>
        </div>
      </div>
    </diV>`;
  }

  run() {
    this.el.innerHTML = this.render();
  }
};

export default Dashboard;
