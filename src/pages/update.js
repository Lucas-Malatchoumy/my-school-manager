import axios from 'axios';

const Update = class {
  constructor(params) {
    this.el = document.querySelector('#app');
    this.params = params;
  }

  updateStudent() {
    const btnUpdate = document.querySelector('#btnUpdate');
    btnUpdate.addEventListener('click', () => {
      const firstName1 = document.querySelector('#firstName').value;
      const lastName1 = document.querySelector('#lastName').value;
      const promo1 = document.querySelector('#promo').value;
      const gender1 = document.querySelector('#gender').value;
      const speciality1 = document.querySelector('#speciality').value;
      const age1 = document.querySelector('#age').value;
      const email1 = document.querySelector('#email').value;
      const notations1 = document.querySelector('#notations').value;
      const data = JSON.stringify({
        firstName: firstName1,
        lastName: lastName1,
        promo: promo1,
        gender: gender1,
        age: age1,
        email: email1,
        notations: notations1,
        speciality: speciality1
      });
      const config = {
        method: 'put',
        url: `https://3f3j7irda8.execute-api.eu-west-3.amazonaws.com/my-school-manager-show/users/update?id=${this.params.id}`,
        headers: {
          'x-api-key': 'eILWZmmK9Eap08ZwchAXS0mGnei11tl43jruta00',
          'Content-Type': 'application/json'
        },
        data
      };

      axios(config)
        .then((response) => {
          console.log(JSON.stringify(response.data));
        })
        .catch((error) => {
          console.log(error);
        });
    });
  }

  render() {
    return `
    <form>
    <div class="row row mb-3">
      <div class="col-md-2 getUser">
        <label> New FirstName</label>
        <input id="firstName" type="text" class="form-control">
      </div>
      <div class="col-md-2 getUser">
        <label>New LastName</label>
        <input id="lastName" type="text" class="form-control">
      </div>
    </div>
    <div class="row row mb-3">
      <div class="col-md-2 getUser">
        <label> New Promo</label>
        <input id="promo" type="text" class="form-control">
      </div>
      <div class="col-md-2 getUser">
            <label>speciality</label>
            <input id="speciality" type="text" class="form-control">
          </div>
        </div>
    <div class="row mb-3">
    <div class="col-md-2 getUser">
      <label>email</label>
      <input id="email" type="text" class="form-control">
    </div>
    <div class="col-md-2 getUser">
      <label>notations</label>
    <input id="notations" type="text" class="form-control">
    </div>
  </div>
  <div class="row mb-3">
    <div class="col-md-2 getUser">
      <label>gender</label>
      <input id="gender" type="text" class="form-control">
    </div>
    <div class="col-md-2 getUser">
      <label>age</label>
      <input id="age" type="text" class="form-control">
    </div>
  </div>
    
    <a href="/student?id=${this.params.id}"><button type="button" id="btnUpdate" class="btn btn-danger getUser mt-3">Update</button></a>
  </form>`;
  }

  run() {
    this.el.innerHTML = this.render();
    this.updateStudent();
  }
};

export default Update;
