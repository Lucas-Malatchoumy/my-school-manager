import axios from 'axios';

const student = class {
  constructor(params) {
    this.el = document.querySelector('#app');
    this.params = params;
  }

  getStudentById() {
    const config = {
      method: 'get',
      url: `https://3f3j7irda8.execute-api.eu-west-3.amazonaws.com/my-school-manager-show/users/show?id=${this.params.id}`,
      headers: {
        'x-api-key': 'eILWZmmK9Eap08ZwchAXS0mGnei11tl43jruta00'
      }
    };

    axios(config)
      .then((response) => {
        const { data } = response;
        this.el.innerHTML = this.render(data);
        console.log(this.params);
        this.deleteStudent();
      });
  }

  deleteStudent() {
    const formEl = document.querySelector('#toto');
    console.log(formEl);
    formEl.addEventListener('click', () => {
      const config = {
        method: 'delete',
        url: `https://3f3j7irda8.execute-api.eu-west-3.amazonaws.com/my-school-manager-show/users/delete?id=${this.params.id}`,
        headers: {
          'x-api-key': 'eILWZmmK9Eap08ZwchAXS0mGnei11tl43jruta00'
        }
      };

      axios(config)
        .then((response) => {
          console.log(response.data);
        })
        .catch((error) => {
          console.log(error);
        });
    });
  }

  render(data) {
    const {
      firstName, lastName, promo, speciality, email, notations, gender, age
    } = data;

    return `
      <h1> Student </h1>
      <form>
        <div class="row mb-3">
          <div class="col-md-2 getUser">
            <label>FirstName</label>
            <input type="text" readonly class="form-control" value="${firstName}">
          </div>
          <div class="col-md-2 getUser">
            <label>LastName</label>
            <input type="text" readonly class="form-control" value="${lastName}">
          </div>
        </div>
        <div class="row mb-3">
          <div class="col-md-2 getUser">
            <label>promo</label>
            <input type="text" readonly class="form-control" value="${promo}">
          </div>
          <div class="col-md-2 getUser">
            <label>speciality</label>
            <input type="text" readonly class="form-control" value="${speciality}">
          </div>
        </div>
        <div class="row mb-3">
        <div class="col-md-2 getUser">
          <label>email</label>
          <input type="text" readonly class="form-control" value="${email}">
        </div>
        <div class="col-md-2 getUser">
          <label>notations</label>
          <input type="text" readonly class="form-control" value="${notations}">
        </div>
      </div>
      <div class="row mb-3">
        <div class="col-md-2 getUser">
          <label>email</label>
          <input type="text" readonly class="form-control" value="${gender}">
        </div>
        <div class="col-md-2 getUser">
          <label>notations</label>
          <input type="text" readonly class="form-control" value="${age}">
        </div>
      </div>
        <a href="/promo?promo=${promo}"<button type="button" id="toto" class="btn btn-danger getUser mt-3">Delete user</button></a>
        <a href="/update?id=${this.params.id}"<button type="button" class="btn btn-success getUser mt-3">Update</button></a>
        <a href="/promo?promo=${promo}"<button type="button" class="btn btn-primary getUser mt-3">Return</button></a>
      </form>`;
  }

  run() {
    this.getStudentById();
  }
};

export default student;
