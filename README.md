# Welcome to MySchool Manager

## Installation
Clone my project.

```bash
git clone https://gitlab.com/Lucas-Malatchoumy/my-school-manager.git
```


Use node v16 use :
```bash
nvm install 16
```

```bash
npm i
```
You have to install axios to request method in api gateway :
```bash
npm i axios
```
## Usage

Start the application dev with :

```bash
npm run start
```

Created the dist with :

```bash
npm run dist
```

Analyse the coding rules with :

```bash
npm run lint
```

### Principes

Welcome to MySchool Manager !

In MySchool Manager, you can supervise your school.

First of all you have to register you with the form inscription.

After that, you will see the Dashboard with your profile, and differents classes.

If you click on a class, you will be able to do differents actions :

-  See all students of a class
-  See all informations of a student
-  Delete a Student
-  Update some information of the student
-  You can disconnect you account
-  In the Dashboard you can search student by their firstname or lastname
